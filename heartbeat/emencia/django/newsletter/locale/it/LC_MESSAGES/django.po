# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: emencia.django.newsletter\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-12-10 03:19-0600\n"
"PO-Revision-Date: 2010-05-26 19:00+0200\n"
"Last-Translator: Paolo Martino <martip@gmail.com>\n"
"Language-Team: martip <martip@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Italian\n"
"X-Poedit-Country: ITALY\n"
"X-Poedit-SourceCharset: utf-8\n"

#: forms.py:18
#, fuzzy
msgid "Email"
msgstr "email"

#: forms.py:41
#, fuzzy
msgid "Mailing lists"
msgstr "liste di distribuzione"

#: models.py:38 models.py:153 models.py:333
msgid "name"
msgstr "nome"

#: models.py:39
msgid "server host"
msgstr "host"

#: models.py:40
msgid "server user"
msgstr "user"

#: models.py:41 models.py:43
msgid "Leave it empty if the host is public."
msgstr "Lascia vuoto se il server è pubblico"

#: models.py:42
msgid "server password"
msgstr "password"

#: models.py:44
msgid "server port"
msgstr "porta"

#: models.py:45
msgid "server use TLS"
msgstr "utilizza TLS"

#: models.py:47
msgid "custom headers"
msgstr "header personalizzati"

#: models.py:48
msgid "key1: value1 key2: value2, splitted by return line."
msgstr "chiave1: valore1 chiave2: valore2, uno per ogni riga"

#: models.py:49
msgid "mails per hour"
msgstr "mail all'ora"

#: models.py:91
msgid "SMTP server"
msgstr "Server SMTP"

#: models.py:92
msgid "SMTP servers"
msgstr "Server SMTP"

#: models.py:97 views/statistics.py:73
msgid "email"
msgstr "email"

#: models.py:98 views/statistics.py:72
msgid "first name"
msgstr "nome"

#: models.py:99 views/statistics.py:72
msgid "last name"
msgstr "cognome"

#: models.py:101
msgid "subscriber"
msgstr "sottoscrittore"

#: models.py:102
msgid "valid email"
msgstr "email valida"

#: models.py:103
msgid "contact tester"
msgstr "contatto di prova"

#: models.py:104
msgid "tags"
msgstr "tag"

#: models.py:110 models.py:162 models.py:222 models.py:255 models.py:319
msgid "creation date"
msgstr "data di creazione"

#: models.py:111 models.py:163 models.py:223
msgid "modification date"
msgstr "data di modifica"

#: models.py:130
msgid "mail format"
msgstr "formato messaggio"

#: models.py:148 models.py:314
msgid "contact"
msgstr "contatto"

#: models.py:149 models.py:336
msgid "contacts"
msgstr "contatti"

#: models.py:154
msgid "description"
msgstr "descrizione"

#: models.py:156 models.py:167
msgid "subscribers"
msgstr "sottoscrittori"

#: models.py:158 models.py:171
msgid "unsubscribers"
msgstr "sottoscrizione rimossa"

#: models.py:183 models.py:206 plugins/models.py:16
msgid "mailing list"
msgstr "lista di distribuzione"

#: models.py:184 models.py:338
msgid "mailing lists"
msgstr "liste di distribuzione"

#: models.py:195
msgid "draft"
msgstr "bozza"

#: models.py:196
msgid "waiting sending"
msgstr "in attesa di invio"

#: models.py:197
msgid "sending"
msgstr "invio in corso"

#: models.py:198 models.py:304
msgid "sent"
msgstr "inviata"

#: models.py:199
msgid "canceled"
msgstr "annullata"

#: models.py:202 models.py:252 models.py:277 plugins/models.py:13
msgid "title"
msgstr "titolo"

#: models.py:203
msgid "content"
msgstr "contenuto"

#: models.py:203
msgid "Or paste an URL."
msgstr "Oppure incolla un URL."

#: models.py:204
msgid "Edit your newsletter here"
msgstr "Modifica la newsletter qui"

#: models.py:207
msgid "test contacts"
msgstr "contatti di prova"

#: models.py:210
msgid "smtp server"
msgstr "server smtp"

#: models.py:212
msgid "sender"
msgstr "mittente"

#: models.py:214
msgid "reply to"
msgstr "rispondi a"

#: models.py:217 models.py:315
msgid "status"
msgstr "stato"

#: models.py:218
msgid "sending date"
msgstr "data di invio"

#: models.py:220
msgid "Used for displaying the newsletter on the site."
msgstr "Usato per mostrare la newsletter sul sito."

#: models.py:245 models.py:276 models.py:313 plugins/cms_plugins.py:13
msgid "newsletter"
msgstr "newsletter"

#: models.py:246 models.py:340
msgid "newsletters"
msgstr "newsletter"

#: models.py:247
msgid "Can change status"
msgstr "Può cambiare lo stato"

#: models.py:253
msgid "url"
msgstr "url"

#: models.py:265 models.py:316
msgid "link"
msgstr "collegamento"

#: models.py:266
msgid "links"
msgstr "collegamenti"

#: models.py:278
msgid "file to attach"
msgstr ""

#: models.py:282
msgid "attachment"
msgstr ""

#: models.py:283
msgid "attachments"
msgstr ""

#: models.py:303
msgid "sent in test"
msgstr "inviata in prova"

#: models.py:305
msgid "error"
msgstr "errore"

#: models.py:306
msgid "invalid email"
msgstr "email non valida"

#: models.py:307
msgid "opened"
msgstr "aperta"

#: models.py:308
msgid "opened on site"
msgstr "aperta sul sito"

#: models.py:309
msgid "link opened"
msgstr "collegamento seguito"

#: models.py:310
msgid "unsubscription"
msgstr "annullamento sottoscrizione"

#: models.py:328
msgid "contact mailing status"
msgstr "stato del contatto"

#: models.py:329
msgid "contact mailing statuses"
msgstr "stati del contatto"

#: models.py:334
msgid "permissions group"
msgstr "permessi del gruppo"

#: models.py:347
msgid "workgroup"
msgstr "gruppo di lavoro"

#: models.py:348
msgid "workgroups"
msgstr "gruppi di lavoro"

#: admin/contact.py:36 templates/newsletter/newsletter_statistics.html:186
msgid "Status"
msgstr "Stato"

#: admin/contact.py:37
msgid "Advanced"
msgstr "Avanzate"

#: admin/contact.py:68
msgid "No relative object"
msgstr "Nessun oggetto correlato"

#: admin/contact.py:70
msgid "Related object"
msgstr "Oggetto correlato"

#: admin/contact.py:77
msgid "Total subscriptions"
msgstr "Sottoscrizioni totali"

#: admin/contact.py:82
msgid "Export contacts as VCard"
msgstr "Esporta i contatti come VCard"

#: admin/contact.py:89
msgid "Export contacts in Excel"
msgstr "Esporta i contatti come Excel"

#: admin/contact.py:94
#, python-format
msgid "New mailinglist at %s"
msgstr "Nuova lista di distribuzione per %s"

#: admin/contact.py:95
#, python-format
msgid "New mailing list created in admin at %s"
msgstr "Nuova lista di distribuzione creata in amministrazione per %s"

#: admin/contact.py:103
#, python-format
msgid "%s succesfully created."
msgstr "%s creata con successo."

#: admin/contact.py:106
msgid "Create a mailinglist"
msgstr "Crea una lista di distribuzione"

#: admin/contact.py:120
#, python-format
msgid "%s contacts succesfully imported."
msgstr "%s contatti importati con successo."

#: admin/contact.py:122
#, fuzzy
msgid "Contact importation"
msgstr "Importazione"

#: admin/mailinglist.py:61
msgid "Please select a least 2 mailing list."
msgstr "Seleziona almento 2 liste di distribuzione."

#: admin/mailinglist.py:73
#, python-format
msgid "Merging list at %s"
msgstr "Unione delle liste per %s"

#: admin/mailinglist.py:74
#, python-format
msgid "Mailing list created by merging at %s"
msgstr "Lista di distribuzione creata per unione per %s"

#: admin/mailinglist.py:79
#, python-format
msgid "%s succesfully created by merging."
msgstr "%s creata con successo per unione."

#: admin/mailinglist.py:82
msgid "Merge selected mailinglists"
msgstr "Unisci le liste di distribuzione selezionate"

#: admin/mailinglist.py:87
msgid "Export Subscribers"
msgstr "Esporta i sottoscrittori"

#: admin/mailinglist.py:89
msgid "Export"
msgstr "Esporta"

#: admin/newsletter.py:37
msgid "Receivers"
msgstr "Destinatari"

#: admin/newsletter.py:38
msgid "Sending"
msgstr "Invio in corso"

#: admin/newsletter.py:39 admin/smtpserver.py:12
msgid "Miscellaneous"
msgstr "Varie"

#: admin/newsletter.py:75
msgid "Default"
msgstr "Predefinito"

#: admin/newsletter.py:99
msgid "Unable to download HTML, due to errors within."
msgstr "Impossibile scaricare l'HTML, a causa di errori nel codice."

#: admin/newsletter.py:112
msgid "View historic"
msgstr "Mostra storico"

#: admin/newsletter.py:113 admin/newsletter.py:122
msgid "Not available"
msgstr ""

#: admin/newsletter.py:115
msgid "Historic"
msgstr "Storico"

#: admin/newsletter.py:121
msgid "View statistics"
msgstr "Mostra statistiche"

#: admin/newsletter.py:124
msgid "Statistics"
msgstr "Statistiche"

#: admin/newsletter.py:134
msgid "Unable send newsletter, due to errors within HTML."
msgstr "Impossibile inviare la newsletter, per errori nel codice HTML."

#: admin/newsletter.py:136
#, python-format
msgid "%s succesfully sent."
msgstr "%s inviata con successo."

#: admin/newsletter.py:138
#, python-format
msgid "No test contacts assigned for %s."
msgstr "Nessun contatto assegnato a %s."

#: admin/newsletter.py:139
msgid "Send test email"
msgstr "Invia email di test"

#: admin/newsletter.py:147
#, python-format
msgid "%s newletters are ready to send"
msgstr "%s newsletter sono pronte per l'invio"

#: admin/newsletter.py:148
msgid "Make ready to send"
msgstr "Prepara per l'invio"

#: admin/newsletter.py:157
#, python-format
msgid "%s newletters are cancelled"
msgstr "%s newsletter sono annullate"

#: admin/newsletter.py:158
msgid "Cancel the sending"
msgstr "Annulla l'invio"

#: admin/smtpserver.py:10
msgid "Configuration"
msgstr "Configurazione"

#: admin/smtpserver.py:33
msgid "Check connection"
msgstr "Verifica connessione"

#: admin/workgroup.py:18
msgid "Contacts length"
msgstr "Lunghezza contatti"

#: admin/workgroup.py:22
msgid "Mailing List length"
msgstr "Lunghezza lista di distribuzione"

#: admin/workgroup.py:26
msgid "Newsletter length"
msgstr "Lunghezza newsletter"

#: plugins/cms_plugins.py:15
#, fuzzy
msgid "Subscription Form"
msgstr "annullamento sottoscrizione"

#: plugins/models.py:14
#, fuzzy
msgid "show description"
msgstr "descrizione"

#: plugins/models.py:15
msgid "Show the mailing list's description."
msgstr ""

#: plugins/models.py:17
#, fuzzy
msgid "Mailing List to subscribe to."
msgstr "Lunghezza lista di distribuzione"

#: templates/admin/newsletter/contact/change_list.html:36
#, python-format
msgid "Add %(name)s"
msgstr "Aggiungi %(name)s"

#: templates/admin/newsletter/contact/change_list.html:42
msgid "Actions"
msgstr "Azioni"

#: templates/admin/newsletter/contact/change_list.html:51
#, fuzzy, python-format
msgid "Import %(name)ss"
msgstr "Importa %(name)s"

#: templates/admin/newsletter/contact/change_list.html:56
msgid "Add to a mailing list"
msgstr "Aggiungi ad una lista"

#: templates/admin/newsletter/contact/change_list.html:63
#, python-format
msgid "Export %(name)s as VCard"
msgstr "Esporta %(name)s come VCard"

#: templates/admin/newsletter/contact/change_list.html:68
#, python-format
msgid "Export %(name)s as Excel"
msgstr "Esporta %(name)s come Excel"

#: templates/newsletter/contact_import.html:7
msgid "Home"
msgstr "Pagina iniziale"

#: templates/newsletter/contact_import.html:10
msgid "Importation"
msgstr "Importazione"

#: templates/newsletter/contact_import.html:24
msgid "Excel"
msgstr ""

#: templates/newsletter/contact_import.html:27
msgid "Excel file (.xls)"
msgstr ""

#: templates/newsletter/contact_import.html:31
#, fuzzy
msgid "Import contacts from a Excel file."
msgstr "Esporta i contatti come Excel"

#: templates/newsletter/contact_import.html:32
msgid "Columns are [email][last name][first name][tags]."
msgstr ""

#: templates/newsletter/contact_import.html:33
#: templates/newsletter/contact_import.html:56
msgid "All columns are optionnal excepting the email."
msgstr ""

#: templates/newsletter/contact_import.html:38
#: templates/newsletter/contact_import.html:61
#: templates/newsletter/contact_import.html:81
#, fuzzy
msgid "Import"
msgstr "Importazione"

#: templates/newsletter/contact_import.html:47
msgid "Text"
msgstr ""

#: templates/newsletter/contact_import.html:50
msgid "Text file (.txt, .csv)"
msgstr ""

#: templates/newsletter/contact_import.html:54
msgid "Import contacts from a text file, or a CSV file."
msgstr ""

#: templates/newsletter/contact_import.html:55
msgid ""
"Columns are [email][last name][first name][tags], splitted by a dot coma."
msgstr ""

#: templates/newsletter/contact_import.html:70
msgid "VCard"
msgstr ""

#: templates/newsletter/contact_import.html:73
msgid "VCard file (.cvf)"
msgstr ""

#: templates/newsletter/contact_import.html:77
msgid ""
"Import contacts from your favorite mail client, by providing a VCard file."
msgstr ""

#: templates/newsletter/mailing_list_subscribe.html:4
#: templates/newsletter/mailing_list_subscribe.html:7
#, fuzzy
msgid "Subscription to mailing list"
msgstr "Aggiungi ad una lista"

#: templates/newsletter/mailing_list_subscribe.html:10
#, fuzzy
msgid "Thanks for your subscription!"
msgstr "Sottoscrizioni totali"

#: templates/newsletter/mailing_list_subscribe.html:16
#, fuzzy
msgid "Validate this form to subscribe to the mailing lists."
msgstr ""
"Conferma questo indirizzo per rimuovere la sottoscrizione a questa lista di "
"distribuzione."

#: templates/newsletter/mailing_list_subscribe.html:21
#: templates/newsletter/cms/subscription_form.html:19
#, fuzzy
msgid "Subscribe"
msgstr "sottoscrittore"

#: templates/newsletter/mailing_list_unsubscribe.html:4
#: templates/newsletter/mailing_list_unsubscribe.html:7
msgid "Unsubscription"
msgstr "Annullamento sottoscrizione"

#: templates/newsletter/mailing_list_unsubscribe.html:9
msgid "You are unsubscribed for this mailing list."
msgstr "La tua sottoscrizione a questa lista di distribuzione è stata rimossa."

#: templates/newsletter/mailing_list_unsubscribe.html:11
msgid "Validate this form to unsubscribe to this mailing list."
msgstr ""
"Conferma questo indirizzo per rimuovere la sottoscrizione a questa lista di "
"distribuzione."

#: templates/newsletter/mailing_list_unsubscribe.html:14
msgid "Unsubscribe"
msgstr "Rimuovi la sottoscrizione"

#: templates/newsletter/newsletter_historic.html:9
#: templates/newsletter/newsletter_statistics.html:68
msgid "Admin."
msgstr "Pagina iniziale"

#: templates/newsletter/newsletter_historic.html:23
msgid "Date"
msgstr "Data"

#: templates/newsletter/newsletter_historic.html:24
msgid "Contact"
msgstr "Contatto"

#: templates/newsletter/newsletter_historic.html:25
msgid "Action"
msgstr "Azione"

#: templates/newsletter/newsletter_statistics.html:80
#: templates/newsletter/newsletter_statistics.html:81
msgid "Broadcasting statistics"
msgstr "Statistiche di diffusione"

#: templates/newsletter/newsletter_statistics.html:83 views/statistics.py:135
msgid "Total openings"
msgstr "Totale aperture"

#: templates/newsletter/newsletter_statistics.html:88
msgid "Openings on site"
msgstr "Aperture sul sito"

#: templates/newsletter/newsletter_statistics.html:93
msgid "Total openings unique"
msgstr "Totale aperture univoche"

#: templates/newsletter/newsletter_statistics.html:98
msgid "Unknow delivery"
msgstr "Stato di recapito sconosciuto"

#: templates/newsletter/newsletter_statistics.html:103
msgid "Unsubscriptions"
msgstr "Annullamento sottoscrizioni"

#: templates/newsletter/newsletter_statistics.html:108
msgid "Openings average"
msgstr "Media aperture"

#: templates/newsletter/newsletter_statistics.html:116
#: templates/newsletter/newsletter_statistics.html:117
msgid "Links statistics"
msgstr "Statistiche collegamenti"

#: templates/newsletter/newsletter_statistics.html:119 views/statistics.py:139
msgid "Total clicked links"
msgstr "Totale collegamenti seguiti"

#: templates/newsletter/newsletter_statistics.html:124
msgid "Total clicked links unique"
msgstr "Totale collegamenti univoci seguiti "

#: templates/newsletter/newsletter_statistics.html:129
msgid "Clicked links by openings"
msgstr "Collegamenti seguiti per apertura"

#: templates/newsletter/newsletter_statistics.html:134
msgid "Clicked links average"
msgstr "Media collegamenti seguiti"

#: templates/newsletter/newsletter_statistics.html:146 views/statistics.py:143
msgid "Consultation histogram"
msgstr "Grafico consultazione"

#: templates/newsletter/newsletter_statistics.html:149
msgid "Period"
msgstr "Periodo"

#: templates/newsletter/newsletter_statistics.html:176
msgid "Report"
msgstr "Report"

#: templates/newsletter/newsletter_statistics.html:178
msgid "Download CSV report"
msgstr "Scarica report in CSV"

#: templates/newsletter/newsletter_statistics.html:183
msgid "Informations"
msgstr "Informazioni"

#: templates/newsletter/newsletter_statistics.html:184
msgid "Recipients"
msgstr "Destinatari"

#: templates/newsletter/newsletter_statistics.html:185
msgid "View"
msgstr "Mostra"

#: templates/newsletter/newsletter_statistics.html:188
msgid "Sending date"
msgstr "Data di invio"

#: templates/newsletter/newsletter_statistics.html:191
msgid "Tests sent"
msgstr "Prove inviate"

#: templates/newsletter/newsletter_statistics.html:196
msgid "Top Links"
msgstr "Collegamenti popolari"

#: templates/newsletter/newsletter_statistics.html:197
msgid "Density map"
msgstr "Mappa densità"

#: templates/newsletter/newsletter_statistics.html:206
msgid "No Top Links yet."
msgstr "Nessun collegamento popolare."

#: templates/newsletter/cms/subscription_form.html:7
#, fuzzy
msgid "You have successfully subscribed to the mailing list!"
msgstr "La tua sottoscrizione a questa lista di distribuzione è stata rimossa."

#: views/statistics.py:52
#, python-format
msgid "Statistics of %s"
msgstr "Statistiche di %s"

#: views/statistics.py:73
msgid "openings"
msgstr "aperture"

#: views/statistics.py:135
msgid "#val# openings"
msgstr "#val# aperture"

#: views/statistics.py:139
msgid "#val# clicks"
msgstr "#val# click"

#: views/tracking.py:45
#, python-format
msgid "Historic of %s"
msgstr "Storico di %s"

#~ msgid "VCard import"
#~ msgstr "Importa VCard"

#~ msgid "Contacts"
#~ msgstr "Contatti"

#~ msgid "Mailing List"
#~ msgstr "Lista di distribuzione"

#~ msgid "Mailing Lists"
#~ msgstr "Liste di distribuzione"

#~ msgid "Newsletter"
#~ msgstr "Newsletter"

#~ msgid "Newsletters"
#~ msgstr "Newsletter"

#~ msgid "Link"
#~ msgstr "Collegamento"

#~ msgid "Links"
#~ msgstr "Collegamenti"

#~ msgid "group"
#~ msgstr "gruppo"

#~ msgid "Connection valid"
#~ msgstr "Connessione valida"

#~ msgid "Display clicks"
#~ msgstr "Mostra i click"

#~ msgid "Unique opening"
#~ msgstr "Aperture univoche"

#~ msgid "Double openings"
#~ msgstr "Doppie aperture"

#~ msgid "Unique click"
#~ msgstr "Click univoci"
